# How to Install

- install docker https://docs.docker.com/get-docker/
- install docker-compose https://docs.docker.com/compose/
- clone this repo
- change dir
```
cd ruslan-karachun-backend-oct2020
```
- create .env file
```
touch .env
```
- run
```
docker-compose up -d --build
```